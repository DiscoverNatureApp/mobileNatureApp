# Mobile App for Learning Nature

## Ambition

Our app aims to give students a new and exiting way to learn about nature. We also want to develop their observational skills and encourage physical activity.

## About the Project

Mobile App for Learning Nature consists of two parts: a web app used by teachers and an [Android app](https://gitlab.com/DiscoverNatureApp/AndroidApp) used by students.

The web interface enables the teacher to create sets of nature related tasks and grade students' answers. Each task represents an object the students must find in nature (e.g. birch tree). For every object a title, description, and a reference image is provided as a clue.

Using the app the students can easily download a specific taskset to their smart device using a password given by the teacher. Multiple students can use the same device by forming groups. Upon finding the object, the task can be answered by taking a picture of the object via the app and submitting it. After the teacher has graded the submissions, the teams are able to see their score in the app.

## Installation Instructions for the Web Application

This project requires [https://nodejs.org/en/download/](Node.js) to be installed on your machine beforehand.

Clone the repository and run the following commands in the project's direcory:

* `npm install`
* `npm start`

Navigate to [http://localhost:3000/](http://localhost:3000/) in your browser.