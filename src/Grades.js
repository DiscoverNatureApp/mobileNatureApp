import React from "react";
import { Table } from "reactstrap";
import * as firebase from "firebase";
import { withRouter } from "react-router-dom";

export class Grades extends React.Component {
  constructor() {
    super();
    this.state = {
      answers: null,
      classes: null,
      groups: null
    };
  }

  componentDidMount() {
    const rootRef = firebase.database().ref();
    const answersRef = rootRef.child("answers");
    answersRef.on("value", snap => {
      this.setState({
        answers: snap.val()
      });
    });

    const classesRef = rootRef.child("classes");
    classesRef.on("value", snap => {
      this.setState({
        classes: snap.val()
      });
    });

    const groupsRef = rootRef.child("groups");
    groupsRef.on("value", snap => {
      this.setState({
        groups: snap.val()
      });
    });
  }

  render() {
    if (firebase.auth().currentUser !== null) {
      var userEmail = firebase.auth().currentUser.email;
    }

    const answers = this.state.answers;
    const classes = this.state.classes;
    const groups = this.state.groups;

    var myGradedGroups = [];
    if (answers && classes && groups) {
      for (var key in answers) {
        // is it user's group's answer?
        var answer = answers[key];
        if (!answer["noOfTasksToGrade"] && (classes[answer["classId"]]["userEmail"] === userEmail)) myGradedGroups.push(answer)
      }
    }

    if (myGradedGroups && answers && classes && groups) {
      console.log(myGradedGroups);
      return (
        <div className="container">
          <h1>Grades</h1>
          <h2>Graded Groups</h2>
          <Table hover size="sm" responsive>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Score</th>
                  <th>Class</th>                  
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {myGradedGroups && myGradedGroups.map(answer => (
                  <tr key={answer.groupId}>
                    <td>{groups[answer.groupId].name}</td>
                    <td>{answer.totalScore}</td>
                    <td>{classes[answer.classId].name}</td>                    
                    <td><button className="btn btn-primary btn-xs" onClick={() => this.props.history.push(`/grades/${answer.groupId}`)}>View Detailed Score</button></td>
                  </tr>
                ))}
              </tbody>
            </Table>
        </div>
      );
    } else {
      return (
        <div className="container">
          <h1>Grades</h1>
          <h2>Graded Groups</h2>
        </div>
      );
    }
  }
}

export default withRouter(Grades);