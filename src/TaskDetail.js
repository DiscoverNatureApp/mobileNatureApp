import React from "react";
import * as firebase from "firebase";

var baseString = null;
var removed = false;

export class TaskDetail extends React.Component {
  constructor() {
    super();
    this.state = {
      data: null
    };
  }

  componentDidMount() {
    const rootRef = firebase.database().ref();
    const taskRef = rootRef.child("tasks").child(this.props.match.params.id);
    taskRef.on("value", snap => {
      this.setState({
        data: snap.val()
      });
    });
  }

  render() {
    if (removed === true) {
      this.props.history.replace("/tasks");
      removed = false;
    }
    const data = this.state.data;
    return (
      <div className="container">
        <h1>Edit Task</h1>
        <div id="errorMessage" role="alert" className="hide alert alert-warning"></div>
        <p>Click on a field to edit.</p>
        <br />
        <p style={{ fontWeight: "bold" }}>Task Name</p>
        <p id="taskName" contentEditable={true}>
          {data ? data.name : ""}
        </p>
        <br />
        <p style={{ fontWeight: "bold" }}>Description</p>
        <p id="taskDescription" contentEditable={true}>
          {data ? data.description : ""}
        </p>
        <br />
        <p style={{ fontWeight: "bold" }}>Current Picture</p>
        <img src={data ? data.picture : ""} alt="Current" height="120" />
        <p className="hide" id="taskPicture" contentEditable={true}>
          {data ? data.picture : ""}
        </p>
        <br />
        <p style={{ fontWeight: "bold" }}>Upload a New Picture</p>
        <input
          type="file"
          id="inputPicture"
          accept="image/*"
          onChange={getBaseUrl}
        />
        <br />
        <p className="hide" id="taskId">
          {data ? data.id : ""}
        </p>
        <br />
        <button className="btn btn" onClick={() => {this.props.history.replace("/tasks")}}>
          Back
        </button>
        &nbsp;&nbsp;
        <button className="btn btn-primary" onClick={editTask}>
          Save
        </button>
        &nbsp;&nbsp;
        <button className="btn btn-danger" onClick={deleteTask}>
          Delete
        </button>
      </div>
    );
  }
}

function editTask() {
  const taskName = document.getElementById("taskName").innerHTML;
  const taskDescription = document.getElementById("taskDescription").innerHTML;
  let taskPicture = document.getElementById("taskPicture").innerHTML;
  const taskId = document.getElementById("taskId").innerHTML;

  const rootRef = firebase.database().ref();
  const taskRef = rootRef.child("tasks");

  if (firebase.auth().currentUser !== null) {
    var userEmail = firebase.auth().currentUser.email;
  }

  if (baseString !== null && baseString !== taskPicture)
    taskPicture = baseString;

  // Check if all required data is present
  // If not, display an error message and don't add task
  const errorMessage = document.getElementById("errorMessage");
  if (!taskName || !taskDescription || !taskPicture) {
    errorMessage.classList.remove("hide");
    errorMessage.innerText = "Fill in all of the fields!";
    return;
  }

  taskRef.child(taskId).set({
    id: taskId,
    name: taskName,
    description: taskDescription,
    userEmail: userEmail,
    picture: taskPicture
  });

  errorMessage.classList.add("hide");
}

function deleteTask() {
  const ref = firebase
    .database()
    .ref()
    .child("tasks")
    .child(document.getElementById("taskId").innerHTML);
  ref.remove();
  removed = true;
}

function getBaseUrl() {
  var file = document.querySelector("input[type=file]")["files"][0];
  getBase64(file);
}

function getBase64(file) {
  var reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = function() {
    baseString = reader.result;
  };
  reader.onerror = function(error) {
    console.log("Error: ", error);
  };
}
