import React from "react";

export class Welcome extends React.Component {

  render() {
    return (
      <div className="container">
        <h1>Welcome!</h1>
        <h2>Our Ambition</h2>
        <p>Mobile Nature App aims to give students a new and exiting way to learn about nature. We also want to develop their observational skills and encourage physical activity.</p>
        <h2>About the Project</h2>
        <p>The project consists of two parts: a web app used by teachers and an <a href="https://gitlab.com/DiscoverNatureApp/AndroidApp">Android app</a> used by students.</p>
        <p>This web interface enables you (the teacher) to create sets of nature related tasks and grade students' answers. Each task represents an object the students must find in nature (e.g. birch tree). For every object a title, description, and a reference image is provided as a clue.</p>
        <p>Using the app the students can easily download a specific taskset to their smart device using a password given by the teacher. Multiple students can use the same device by forming groups. Upon finding the object, the task can be answered by taking a picture of the object via the app and submitting it. After the teacher has graded the submissions, the teams are able to see their score in the app.</p>
      </div>
    );
  }
}
