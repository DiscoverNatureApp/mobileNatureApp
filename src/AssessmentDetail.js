import React from "react";
import { Table } from "reactstrap";
import * as firebase from "firebase";

export class AssessmentDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tasksToGrade: null,
      tasks: null,
      answers: null
    };
  }

  componentDidMount() {
    const rootRef = firebase.database().ref();
    const tasksToGradeRef = rootRef.child("tasksToGrade");
    const tasksRef = rootRef.child("tasks");
    const answersRef = rootRef.child("answers");

    tasksToGradeRef.on("value", snap => {
      this.setState({
        tasksToGrade: snap.val()
      });
    });

    tasksRef.on("value", snap => {
      this.setState({
        tasks: snap.val()
      })
    })

    answersRef.on("value", snap => {
      this.setState({
        answers: snap.val()
      })
    })
  }

  render() {
    const tasks = this.state.tasks;
    const tasksToGrade = this.state.tasksToGrade;
    const answers = this.state.answers;
    const taskId = this.props.match.params.id;

    var isLastAnswerToGrade = false;
    var groupId = null; // selected randomly
    var groupIdDBKey = null;
    var answersNum = 0;

    if (taskId && tasks && tasksToGrade && tasksToGrade[taskId]) {
      const numOfAnswers = Object.keys(tasksToGrade[taskId]).length;
      answersNum = numOfAnswers;
      if (numOfAnswers === 1) isLastAnswerToGrade = true;
      const randomIndex = Math.floor(Math.random() * numOfAnswers);
      // console.log(randomIndex);
      var indexCounter = 0;
      for (var key in tasksToGrade[taskId]) {
        // console.log(tasksToGrade[taskId][key].groupId);
        if (indexCounter === randomIndex) {
          groupId = tasksToGrade[taskId][key].groupId;
          groupIdDBKey = key;
        }
        indexCounter += 1;
      }
    }

    if (groupId && groupIdDBKey && answers && answers[groupId]) {
      return (
        <div className="container">
          <h1>Grading</h1>
          <Table size="sm" responsive style={{textAlign: 'center'}}>
            <thead>
              <tr>
                <th><center>Answer Picture</center></th>
                <th><center>Reference Picture</center></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><img src={"data:image/png;base64," + answers[groupId]["results"][taskId]} alt="answer" height="350" /></td>
                <td><img src={tasks[taskId].picture} alt="reference" height="350" /></td>
              </tr>
              <tr>
                <td colSpan="2">
                  <br />
                  <button className="btn btn-danger" onClick={() => {saveGrade("wrong", taskId, groupId, groupIdDBKey); if (!isLastAnswerToGrade) this.forceUpdate(); else this.props.history.replace("/assessment")}}>
                    Wrong
                  </button>
                  &nbsp;&nbsp;
                  <button className="btn btn-secondary" onClick={() => {saveGrade("cryptic", taskId, groupId, groupIdDBKey); if (!isLastAnswerToGrade) this.forceUpdate(); else this.props.history.replace("/assessment")}}>
                    Can't Tell
                  </button>
                  &nbsp;&nbsp;
                  <button className="btn btn-success" onClick={() => {saveGrade("correct", taskId, groupId, groupIdDBKey); if (!isLastAnswerToGrade) this.forceUpdate(); else this.props.history.replace("/assessment")}}>
                    Correct
                  </button>
                </td>
              </tr>
            </tbody>
          </Table>

          <h2>Task Info</h2>
          <Table size="sm" responsive>
            <tbody>
              <tr>
                <th scope="row">Name</th>
                <td>{tasks[taskId].name}</td>
              </tr>
              <tr>
                <th scope="row">Description</th>
                <td>{tasks[taskId].description}</td>
              </tr>
              <tr>
                <th scope="row"># of Answers To Grade</th>
                <td>{answersNum}</td>
              </tr>
            </tbody>
          </Table>
        </div>
      );
    } else {
      return (
        <div className="container"><h1>Grading</h1></div>
      )
    }
  }
}

function saveGrade(grade, taskId, groupId, groupIdDBKey) {
  const rootRef = firebase.database().ref();
  const answersRef = rootRef.child("answers");
  const tasksToGradeRef = rootRef.child("tasksToGrade");
  const gradesRef = answersRef.child(groupId + "/grades");
  const totalScoreRef = answersRef.child(groupId + "/totalScore");
  const noOfTasksToGradeRef = answersRef.child(groupId + "/noOfTasksToGrade");

  // remove group's answered task from tasksToGrade
  tasksToGradeRef.child(taskId + "/" + groupIdDBKey).remove()
  .then(function() {
    console.log("Remove succeeded.")
  })
  .catch(function(error) {
    console.log("Remove failed: " + error.message)
  });

  // decrement noOfTasksToGrade
  noOfTasksToGradeRef.transaction(function(noOfTasks) {
    if (noOfTasks) {
      noOfTasks = noOfTasks - 1;
    }
    return noOfTasks;
  });
  
  // set the grade
  var gradeObj = {};
  gradeObj[taskId] = grade;
  gradesRef.update(gradeObj);

  // update totalScore
  totalScoreRef.transaction(function(totalScore) {
    if (grade === "correct") {
      totalScore = (totalScore || 0) + 1;
    }
    return totalScore;
  });
}