import React from "react";
import { Table } from "reactstrap";
import * as firebase from "firebase";

export class GradesDetail extends React.Component {
  constructor() {
    super();
    this.state = {
      answers: null,
      classes: null,
      groups: null,
      tasks: null
    };
  }

  componentDidMount() {
    const rootRef = firebase.database().ref();
    const answersRef = rootRef.child("answers");
    answersRef.on("value", snap => {
      this.setState({
        answers: snap.val()
      });
    });

    const classesRef = rootRef.child("classes");
    classesRef.on("value", snap => {
      this.setState({
        classes: snap.val()
      });
    });

    const groupsRef = rootRef.child("groups");
    groupsRef.on("value", snap => {
      this.setState({
        groups: snap.val()
      });
    });

    const tasksRef = rootRef.child("tasks");
    tasksRef.on("value", snap => {
      this.setState({
        tasks: snap.val()
      });
    }); 
  }

  render() {
    // if (firebase.auth().currentUser !== null) {
    //   var userEmail = firebase.auth().currentUser.email;
    // }

    const answers = this.state.answers;
    const classes = this.state.classes;
    const groups = this.state.groups;
    const tasks = this.state.tasks;
    const groupId = this.props.match.params.id;

    if (answers && classes && groups && tasks && groupId) {
      return (
        <div className="container">
          <h1>Detailed Score</h1>

          <h2>Class Info</h2>
          <Table size="sm" responsive>
            <tbody>
              <tr>
                <th scope="row">Name</th>
                <td>{classes[answers[groupId]["classId"]]["name"]}</td>
              </tr>
              <tr>
                <th scope="row">Time Limit</th>
                <td>{classes[answers[groupId]["classId"]]["timeLimit"]} minutes</td>
              </tr>
            </tbody>
          </Table>
          
          <h2>Group Info</h2>
          <Table size="sm" responsive>
            <tbody>
              <tr>
                <th scope="row">Name</th>
                <td>{groups[groupId]["name"]}</td>
              </tr>
              <tr>
                <th scope="row">Score</th>
                <td><strong><big>{answers[groupId]["totalScore"]}</big></strong></td>
              </tr>
              
              <tr>
                <th scope="row">Photo</th>
                <td>
                  <img src={"data:image/png;base64," + groups[groupId]["photo"]} alt="not submitted" height="300" />
                </td>
              </tr>
            </tbody>
          </Table>
          <h3>Members</h3>
          <Table size="sm" responsive>
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>              
              </tr>
            </thead>
            <tbody>
              {Object.keys(groups[groupId]["members"]).map(index => (
                  <tr key={index}>
                    <td>{parseInt(index, 10)+1}</td>
                    <td>{groups[groupId]["members"][index]}</td>
                  </tr>
                ))}
            </tbody>
          </Table>

          <h2>Answers</h2>
          <Table hover size="sm" responsive>
            <thead>
              <tr>
                <th>Task Name</th>
                <th>Reference Picture</th>
                <th>Answer Picture</th>
                <th>Grade</th>                  
              </tr>
            </thead>
            <tbody>
              {Object.keys(answers[groupId]["grades"]).map(taskId => (
                <tr key={taskId}>
                  <td>{tasks[taskId].name}</td>
                  <td><img src={tasks[taskId]["picture"]} alt="reference" height="100" /></td>
                  <td><img src={"data:image/png;base64," + answers[groupId]["results"][taskId]} alt="answer" height="100" /></td>
                  <td>{answers[groupId]["grades"][taskId]}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      );
    } else {
      return (
        <div className="container">
          <h1>Detailed Score</h1>
        </div>
      );
    }
  }
}