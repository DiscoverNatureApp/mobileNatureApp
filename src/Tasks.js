import React from "react";
import { Table } from "reactstrap";
import * as firebase from "firebase";
import { withRouter } from "react-router-dom";

var baseString=null;

export class Tasks extends React.Component {
  constructor() {
    super();
    this.state = {
      data: null
    };
  }

  componentDidMount() {
    const rootRef = firebase.database().ref();
    const taskRef = rootRef.child("tasks");
    taskRef.on("value", snap => {
      this.setState({
        data: snap.val()
      });
    });
  }

  render() {
    if (firebase.auth().currentUser !== null) {
      var userEmail = firebase.auth().currentUser.email;
    }

    const data = this.state.data;
    var taskData = [];

    //To be able to map data later
    if (data) {
      for (var key in data) {
        var dataEmail = data[key].userEmail;
        if (dataEmail === userEmail) taskData.push(data[key]);
      }
    }

    return (
      <div className="container">
        <h1>Tasks</h1>
        <form className="form-horizontal">
          <fieldset>
            <h2>Add Task</h2>
            <div id="errorMessage" role="alert" className="hide alert alert-warning"></div>
            
            <div className="form-group">
              <label className="col-lg-2 control-label">Name</label>
              <div className="col-lg-10">
                <input
                  type="text"
                  className="form-control"
                  id="inputName"
                  placeholder="Name"
                  required
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-2 control-label">Description</label>
              <div className="col-lg-10">
                <textarea
                  type="text"
                  className="form-control"
                  id="inputDescription"
                  placeholder="Description"
                  required
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-2 control-label">Picture</label>
              <div className="col-lg-10 col-lg-offset-10,5">
                <input type="file" id="inputPicture" accept="image/*" onChange={getBaseUrl} required/>
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-2 control-label"></label>
              <div className="col-lg-10">
                <button
                  type="submit"
                  className="btn btn-primary"
                  onClick={saveTask}
                >
                  Submit
                </button>
              </div>
            </div>

          </fieldset>
        </form>

        <div>
          <h2>Your Tasks</h2>
          <Table hover size="sm" responsive>
            <thead>
              <tr>
                <th>Name</th>
                <th>Description</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {taskData &&
                taskData.map(task => (
                  <tr>
                    <td>{task.name}</td>
                    <td>{task.description}</td>
                    <td><button class="btn btn-primary btn-xs" onClick={() => this.props.history.push(`/tasks/${task.id}`)} key={task.id}>Edit</button></td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

function saveTask() {

  // Check if all required data is present
  // If not, display an error message and don't add task
  const errorMessage = document.getElementById("errorMessage");
  if (!document.getElementById("inputName").value || !document.getElementById("inputDescription").value || !baseString) {
    errorMessage.classList.remove("hide");
    errorMessage.innerText = "Fill in all of the fields!";
    return;
  }

  const rootRef = firebase.database().ref();
  const taskRef = rootRef.child("tasks");

  var myRef = taskRef.push();
  var key = myRef.key;

  if (firebase.auth().currentUser !== null) {
    var userEmail = firebase.auth().currentUser.email;
  }

      try {
        taskRef.child(key).set({
          id: key,
          name: document.getElementById("inputName").value,
          description: document.getElementById("inputDescription").value,
          userEmail: userEmail,
          picture: baseString
        });
      } catch (err) {
        window.alert(err.message);
      }
}

function getBaseUrl()  {
  var file    = document.querySelector('input[type=file]')['files'][0];
  getBase64(file);
}

function getBase64(file) {
  var reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = function () {
    baseString = reader.result
  };
  reader.onerror = function (error) {
    console.log('Error: ', error);
  };
}

export default withRouter(Tasks);
