import React from "react";
import { Table } from "reactstrap";
import * as firebase from "firebase";
import { withRouter } from "react-router-dom";

export class Assessment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tasksToGrade: null,
      tasks: null
    };
  }

  componentDidMount() {
    const rootRef = firebase.database().ref();
    const tasksToGradeRef = rootRef.child("tasksToGrade");
    tasksToGradeRef.on("value", snap => {
      this.setState({
        tasksToGrade: snap.val()
      });
    });

    const tasksRef = rootRef.child("tasks");
    tasksRef.on("value", snap => {
      this.setState({
        tasks: snap.val()
      })
    })
  }

  render() {
    if (firebase.auth().currentUser !== null) {
      var userEmail = firebase.auth().currentUser.email;
    }

    const tasks = this.state.tasks;
    const tasksToGrade = this.state.tasksToGrade;

    var usersTasksIds = [];
    var usersTaskIdsToGrade = [];
    
    var allDataFetched = false;
    if (tasks && tasksToGrade && userEmail) {
      for (var taskId1 in tasks) if (tasks[taskId1].userEmail === userEmail) usersTasksIds.push(taskId1);
      for (var taskId2 in tasksToGrade) {
        if (usersTasksIds.indexOf(taskId2) >= 0) {
          usersTaskIdsToGrade.push(taskId2);
        }
      }
      allDataFetched = true;
    }

    return (
      <div className="container">
        <h1>Assessment</h1>
        <h2>Answered Tasks</h2>
        <Table hover size="sm" responsive>
          <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th># of Answers To Grade</th>
              {<th></th>}
            </tr>
          </thead>
          <tbody>
            {allDataFetched &&
              usersTaskIdsToGrade.map(taskId => (
                <tr key={taskId}>
                  <td>{tasks[taskId].name}</td>
                  <td>{tasks[taskId].description}</td>
                  <td>{Object.keys(tasksToGrade[taskId]).length}</td>
                  <td><button className="btn btn-primary btn-xs" onClick={() => this.props.history.push(`/assessment/${taskId}`)}>Start Grading</button></td>
                </tr>
              ))}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default withRouter(Assessment);