import React, { Component } from "react";
import * as firebase from "firebase";

var mySet = new Set();
var removed = false;

export class ClassDetail extends Component {
  constructor() {
    super();
    this.state = {
      data: null,
      tasksInClassData: null,
      myClass: null
    };
  }

  componentDidMount() {
    const rootRef = firebase.database().ref();
    const taskRef = rootRef.child("tasks");
    taskRef.on("value", snap => {
      this.setState({
        data: snap.val()
      });
    });

    const classRef = rootRef.child("classes").child(this.props.match.params.id);
    classRef.on("value", snap => {
      this.setState({
        myClass: snap.val()
      });
    });

    const rootRef2 = firebase.database().ref();
    const taskRef2 = rootRef2
      .child("tasksInClass")
      .child(this.props.match.params.id);
    taskRef2.on("value", snap => {
      this.setState({
        tasksInClassData: snap.val()
      });
    });
  }

  toggleCheckbox = label => {
    const value = label.target.value;
    if (mySet.has(value)) {
      mySet.delete(value);
    } else {
      mySet.add(value);
    }
  };

  handleFormSubmit = formSubmitEvent => {
    formSubmitEvent.preventDefault();

    const id = this.props.match.params.id;

    const rootRef = firebase.database().ref();
    const ref = rootRef.child("tasksInClass");

    ref.child(id).set({
      sample: "sample"
    });

    for (const checkbox of mySet) {
      ref
        .child(id)
        .child(checkbox)
        .set({
          taskId: checkbox
        });
    }

    const className = document.getElementById("className").innerHTML;
    const classPassword = document.getElementById("classPassword").innerHTML;
    const classTimeLimit = document.getElementById("classTimeLimit").innerHTML;
    const classId = document.getElementById("classId").innerHTML;

    const classRef = rootRef.child("classes");

    if (firebase.auth().currentUser !== null) {
      var userEmail = firebase.auth().currentUser.email;
    }

    // Check if all required data is present
    // If not, display an error message and don't add task
    const errorMessage = document.getElementById("errorMessage");
    if (!className || !classPassword || !classTimeLimit) {
      errorMessage.classList.remove("hide");
      errorMessage.innerText = "Fill in all of the fields!";
      return;
    }

    classRef.child(classId).set({
      id: classId,
      name: className,
      passw: classPassword,
      timeLimit: classTimeLimit,
      userEmail: userEmail
    });

    errorMessage.classList.add("hide");
  };

  createCheckbox = label => (
    <div key={label.id}>
      <input
        type="checkbox"
        id={label.id}
        name={label.name}
        value={label.id}
        defaultChecked={label.isTask}
        onChange={this.toggleCheckbox}
      />{" "}
      {label.name}
      <br />
    </div>
  );

  render() {
    if (removed === true) {
      this.props.history.replace("/classes");
      removed = false;
    }

    var finalData = [];
    var finalData2 = [];
    const myClass = this.state.myClass;

    if (firebase.auth().currentUser !== null) {
      var userEmail = firebase.auth().currentUser.email;
    }

    const data = this.state.data;
    const tasksInClassData = this.state.tasksInClassData;

    for (var key3 in this.state.tasksInClassData) {
      mySet.add(key3);
    }

    if (tasksInClassData) {
      for (var key2 in tasksInClassData) {
        finalData2.push(key2);
      }
    }

    if (data && tasksInClassData) {
      for (var key in data) {
        var dataEmail = data[key].userEmail;
        if (dataEmail === userEmail) {
          const bool = finalData2.indexOf(data[key].id) > -1;
          const obj = { name: data[key].name, id: data[key].id, isTask: bool };
          finalData.push(obj);
        }
      }
    }

    return (
      <div className="container">
        <h1>Edit Class</h1>
        <div id="errorMessage" role="alert" className="hide alert alert-warning"></div>
        <p>Click on a field to edit.</p>
        <br />
        <p style={{ fontWeight: "bold" }}>Class Name</p>
        <p id="className" contentEditable={true}>
          {myClass ? myClass.name : ""}
        </p>
        <br />
        <p style={{ fontWeight: "bold" }}>Password</p>
        <p id="classPassword" contentEditable={true}>
          {myClass ? myClass.passw : ""}
        </p>
        <br />
        <p style={{ fontWeight: "bold" }}>Time Limit in Minutes</p>
        <p id="classTimeLimit" contentEditable={true}>
          {myClass ? myClass.timeLimit : ""}
        </p>
        <br />
        <p className="hide" id="classId">
          {myClass ? myClass.id : ""}
        </p>
        <p style={{ fontWeight: "bold" }}>Tasks in Class</p>
        <div className="row">
          <div className="col-sm-12">
            <form onSubmit={this.handleFormSubmit}>
              {finalData ? finalData.map(this.createCheckbox) : null}
              <br />
              <button className="btn btn" onClick={() => {this.props.history.replace("/classes")}}>
                Back
              </button>
              &nbsp;&nbsp;
              <button className="btn btn-primary" type="submit">
                Save
              </button>
              &nbsp;&nbsp;
              <button className="btn btn-danger" onClick={deleteClass}>
                Delete
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

function deleteClass() {
  const ref = firebase
    .database()
    .ref()
    .child("classes")
    .child(document.getElementById("classId").innerHTML);
  ref.remove();
  removed = true;
}
